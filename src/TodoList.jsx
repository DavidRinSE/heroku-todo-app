import React, { Component } from 'react';
import TodoItem from './TodoItem.jsx';

class TodoList extends Component {
    render() {
    return (
        <section className="main">
        <ul className="todo-list">
            {this.props.todos.map(todo => (
                <TodoItem todo={todo} toggle={this.props.toggle} delete={this.props.delete} key={todo.id}/>
            ))}
        </ul>
        </section>
    );
    }
}

export default TodoList;