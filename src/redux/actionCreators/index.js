import {domain} from "./constants"
import { GET_TODOS,  POST_TODOS,  DELETE_TODOS, CLEAR_COMPLETED, TOGGLE_TODOS} from "../actionTypes"

export const getTodos = () => dispatch => {
    dispatch({
        type: GET_TODOS.START,
    })

    fetch(domain + "/tasks")
        .then(res => res.json())
        .then(json => {
            if(json.statusCode === 200){
                dispatch({
                    type: GET_TODOS.SUCCESS,
                    payload: json
                })
            } else {
                dispatch({
                    type: GET_TODOS.FAIL,
                    payload: json
                })
            }
        })
        .catch(() => {
            dispatch({
                type: GET_TODOS.FAIL,
                payload: "fail"
            })
        })
}

export const postTodo = (task) => dispatch => {
    dispatch({
        type: POST_TODOS.START,
    })
    console.log(JSON.stringify({task: task}))
    
    fetch(domain + "/tasks", {
        method: 'POST',
        body: JSON.stringify({task: task})
    })
        .then(res => res.json())
        .then(response => {
            if(response.statusCode === 201){
                dispatch({
                    type: POST_TODOS.SUCCESS,
                    payload: response
                })
                dispatch(getTodos())
            } else {
                dispatch({
                    type: POST_TODOS.FAIL,
                    payload: response
                })
            }
        })
}

export const deleteTodo = (id) => dispatch => {
    dispatch({
        type: DELETE_TODOS.START,
    })
    fetch(domain + "/tasks/" + id, {
        method: "DELETE"
    }).then(res => res.json()).then(response => {
        if(response.statusCode === 200){
            dispatch({
                type: DELETE_TODOS.SUCCESS,
                payload: response
            })
            dispatch(getTodos())
        } else {
            dispatch({
                type: DELETE_TODOS.FAIL,
                payload: response
            })
        }
    })
}

export const toggleTodo = (id, isactive) => dispatch => {
    dispatch({
        type: TOGGLE_TODOS.START,
    })
    fetch(domain + "/tasks/" + id, {
        method: 'PUT',
        body: JSON.stringify({isactive: !isactive})
    })
        .then(res => res.json())
        .then(response => {
            if(response.statusCode === 200){
                dispatch({
                    type: TOGGLE_TODOS.SUCCESS,
                    payload: response
                })
                dispatch(getTodos())
            } else {
                dispatch({
                    type: TOGGLE_TODOS.FAIL,
                    payload: response
                })
            }
        })
}

export const clearCompleted = () => dispatch => {
    dispatch({
        type: CLEAR_COMPLETED.START,
    })
    fetch(domain + "/tasks/clearCompleted", {
        method: "DELETE"
    }).then(res => res.json()).then(response => {
        if(response.statusCode === 200){
            dispatch({
                type: CLEAR_COMPLETED.SUCCESS,
                payload: response
            })
            dispatch(getTodos())
        } else {
            dispatch({
                type: CLEAR_COMPLETED.FAIL,
                payload: response
            })
        }
    })
}