export const domain = "https://davidr-todo.herokuapp.com/api"

export const jsonHeaders = {
    "Content-Type": "application/json",
    Accept: "application/json"
};