const createActionTypes = actionName => {
    const ACTIONNAME = actionName.toUpperCase();
    return {
        START: ACTIONNAME + ".START",
        SUCCESS: ACTIONNAME + ".SUCCESS",
        FAIL: ACTIONNAME + ".FAIL",
    };
};

export const GET_TODOS = createActionTypes("GET_TODOS")
export const POST_TODOS = createActionTypes("POST_TODOS")
export const DELETE_TODOS = createActionTypes("DELETE_TODOS")
export const TOGGLE_TODOS = createActionTypes("TOGGLE_TODOS")
export const CLEAR_COMPLETED = createActionTypes("CLEAR_COMPLETED")