import { GET_TODOS,  POST_TODOS,  DELETE_TODOS, CLEAR_COMPLETED, TOGGLE_TODOS} from "../actionTypes"

const getTodosDefault = {
    res: null,
    loading: false,
    error: null
}
export function getTodos(state = getTodosDefault, action){
    switch (action.type){
        case GET_TODOS.START:
            return {...state, loading: true}
        case GET_TODOS.SUCCESS:
            return {
                res: action.payload,
                loading: false,
                error: null
            }
        case GET_TODOS.FAIL:
            return {
                res: null,
                loading: false,
                error: action.payload
            }
        default:
            return state
    }
}

const defaultState = {
    res: null,
    loading: false,
    error: null
}
export function postTodo(state = {...defaultState}, action){
    switch(action.type){
        case POST_TODOS.START:
            return {...state, loading: true}
        case POST_TODOS.SUCCESS:
            return {res: action.payload, loading: false, error: null}
        case POST_TODOS.FAIL:
            return {res:null, loading: false, error: action.payload}
        default:
            return state
    }
}
export function deleteTodo(state = {...defaultState}, action){
    switch(action.type){
        case DELETE_TODOS.START:
            return {...state, loading: true}
        case DELETE_TODOS.SUCCESS:
            return {res: action.payload, loading: false, error: null}
        case DELETE_TODOS.FAIL:
            return {res: null, loading: false, error: action.payload}
        default:
            return state
    }
}
export function toggleTodo(state = {...defaultState}, action){
    switch(action.type){
        case TOGGLE_TODOS.START:
            return {...state, loading: true}
        case TOGGLE_TODOS.SUCCESS:
            return {res: action.payload, loading: false, error: null}
        case TOGGLE_TODOS.FAIL:
            return {res: null, loading: false, error: action.payload}
        default:
            return state
    }
}

export function clearCompletedTodos(state = {...defaultState}, action){
    switch(action.type){
        case CLEAR_COMPLETED.START:
            return {...state, loading: true}
        case CLEAR_COMPLETED.SUCCESS:
            return {res: action.payload, loading: false, error: null}
        case CLEAR_COMPLETED.FAIL:
            return {res: null, loading: false, error: action.payload}
        default:
            return state
    }
}