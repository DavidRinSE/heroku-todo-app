import { combineReducers } from 'redux';
import {getTodos, postTodo, deleteTodo, toggleTodo, clearCompletedTodos} from "./tasks"

export default combineReducers({
    getTodos,
    postTodo,
    deleteTodo,
    toggleTodo,
    clearCompletedTodos
})