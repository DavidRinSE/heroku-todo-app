import React, { Component } from "react";
import "./index.css";
import TodoList from './TodoList.jsx';
import { Route, NavLink as Link } from "react-router-dom";
import { connect } from 'react-redux';
import { getTodos, postTodo, clearCompleted } from "./redux/actionCreators"
//import { addTodo, clearCompleted} from './actions.js';

class App extends Component {
  state = {
    textValue:""
  }

  handleKeyDown = (event) =>{
    if(event.key === "Enter"){
      this.props.postTodo(this.state.textValue)
      this.setState({textValue: ""})
    }
  }

  componentDidMount(){
    this.props.getTodos()
  }

  itemsLeft(tasks){
    return tasks.filter(todo => todo.isactive === true).length
  }

  render() {
    let tasks = []
    if(this.props.todos && this.props.todos.res){
      tasks = this.props.todos.res.tasks
    }
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            onChange={(event)=>{this.setState({textValue: event.target.value})}}
            value={this.state.textValue}
            onKeyDown={this.handleKeyDown}
            autoFocus
          />
        </header>
        <Route exact path="/" render={()=>(<TodoList todos={tasks} toggle={this.toggleItem} delete={this.deleteItem}/>)}/>
        <Route exact path="/active" render={()=>(<TodoList todos={tasks.filter((item) => item.isactive === true)} toggle={this.toggleItem} delete={this.deleteItem}/>)}/>
        <Route exact path="/completed" render={()=>(<TodoList todos={tasks.filter((item)=>item.isactive === false)} toggle={this.toggleItem} delete={this.deleteItem}/>)}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.itemsLeft(tasks)}</strong> item(s) left
          </span>
          <ul className="filters">
            <li>
              <Link exact to="/" activeClassName="selected">All</Link>
            </li>
            <li>
              <Link exact to="/active" activeClassName="selected">Active</Link>
            </li>
            <li>
              <Link exact to="/completed" activeClassName="selected">Completed</Link>
            </li>
          </ul>
          {/* onClick={this.props.clearCompleted} */}
          <button onClick={this.props.clearCompleted} className="clear-completed" >Clear completed</button>
        </footer>
        {(this.props.todos && this.props.todos.error) && <p>Error loading todos</p>}
        {(this.props.postTodoState && this.props.postTodoState.error) && <p>Failed to post todo</p>}
        {(this.props.deleteTodoState && this.props.deleteTodoState.error) && <p>Failed to delete todo</p>}
        {(this.props.toggleTodoState && this.props.toggleTodoState.error) && <p>Failed to toggle todo</p>}
        {(this.props.clearTodosState && this.props.clearTodosState.error) && <p>Failed to clear inactive todos</p>}
      </section>
    );
  }
}
const mapStateToProps = (state) => ({todos:state.getTodos, postTodoState: state.postTodo, deleteTodoState: state.deleteTodo, toggleTodoState: state.todoItem, clearTodosState: state.clearCompletedTodos})
const mapDispatchToProps = {
  getTodos,
  postTodo,
  clearCompleted
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
