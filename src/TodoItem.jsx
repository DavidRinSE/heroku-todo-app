import React, { Component } from 'react';
import { connect } from 'react-redux';
import {deleteTodo, toggleTodo} from './redux/actionCreators'
//import { deleteTodo, toggleTodo } from './actions.js';


class TodoItem extends Component {
    render() {
    return (
        <li className={this.props.todo.isactive ? "" : "completed"}>
            <div className="view">
            <input
                className="toggle"
                type="checkbox"
                checked={!this.props.todo.isactive}
                onChange={()=>{this.props.toggleTodo(this.props.todo.id, this.props.todo.isactive)}}
            />
            <label>{this.props.todo.task}</label>
            <button className="destroy" onClick={()=>{this.props.deleteTodo(this.props.todo.id)}} />
            </div>
        </li>
        );
    }
}

const mapDispatchToProps = {
    deleteTodo,
    toggleTodo
}

export default connect(null, mapDispatchToProps)(TodoItem);